package com.evenstein.treater.controllers;

import com.evenstein.treater.models.Message;
import com.evenstein.treater.repos.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class MainController {

    @Autowired
    private MessageRepository message;

    private static final String MESSAGES = "messages";

    @GetMapping("")
    public String index() {
        return "index";
    }

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World")
                                       String name, Map<String, Object> model) {
        model.put("name", name);
        return "greeting";
    }

    @GetMapping("/main")
    public String getMain(@RequestParam(required = false, defaultValue = "") String filter,
                          Model model) {
        Iterable<Message> messages;

        if (filter == null || filter.isEmpty()) {
            messages = this.message.findAll();
        } else {
            messages = message.findByTag(filter);
        }
        model.addAttribute(MESSAGES, messages);
        model.addAttribute("filter", filter);
        return "main";
    }

    @PostMapping("/main")
    public String addMessage(@RequestParam String text, @RequestParam String tag,
                             Map<String, Object> map) {
        Message mess = new Message(text, tag);
        this.message.save(mess);

        Iterable<Message> messages = message.findAll();
        map.put(MESSAGES, messages);

        return "main";
    }
}
