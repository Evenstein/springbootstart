package com.evenstein.treater.models;

public enum Role {
    USER,
    ADMIN
}
