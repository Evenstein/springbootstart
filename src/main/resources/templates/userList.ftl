<#import "parts/common.ftl" as c>

<@c.page>
<div align="center">User's list:</div>
<br/>
<table align="center" border="1" cellpadding="10" width="300">
    <thead align="center">
    <tr>
        <td><b>ID</b></td>
        <td><b>Username</b></td>
        <td><b>Role</b></td>
        <td></td>
    </tr>
    </thead>
    <tbody align="center">
<#list users as user>
<tr>
    <td><p>${user.id}</p></td>
    <td><i>${user.username}</i></td>
    <td>
        <i>
            <#list user.roles as role>
                <div>${role}</div>
            </#list>
        </i>
    </td>
    <td><a href="/user/${user.id}">Edit</a></td>
</tr>
<#else>
    <div>User's list is empty!</div>
</#list>
    </tbody>
</table>
</@c.page>