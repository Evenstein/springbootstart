<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
Add new user
<br />
    ${message?if_exists}
<br />
    <@l.login "/registration" />
</@c.page>