<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
Login page
<br />
<@l.login "/login" />
<br />
<div class="btn btn-danger">
    <a href="/registration">Add new User</a>
</div>
</@c.page>