<#import "parts/common.ftl" as c>
<#import "parts/login.ftl" as l>

<@c.page>
    <div align="center">
        <div>
        <@l.logout />
        </div>
        <span><a class="btn btn-info" href="/user">User's List</a></span>
        <br/>
        <div>
            <form method="post" action="">
                <input type="text" name="text" placeholder="Введите текст сообщения"/>
                <input type="text" name="tag" placeholder="Тэг"/>
                <input type="hidden" name="_csrf" value="${_csrf.token}"/>
                <button type="submit">Добавить сообщение</button>
            </form>
        </div>
        <br/>
        <div>
            <form method="get" action="/main">
                <input type="text" name="filter" value="${filter}"/>
                <button type="submit">Найти</button>
            </form>
        </div>
        <br/>
        <div>Messages:</div>
        <table width="400" border="1">
            <thead align="center">
                <tr>
                    <td><b>ID</b></td>
                    <td><b>Message</b></td>
                    <td><b>Tag</b></td>
                    <td></td>
                </tr>
            </thead>
            <tbody align="center">
                <#list messages as message>
                    <tr>
                        <td><b>${message.id}</b></td>
                        <td><span>${message.text}</span></td>
                        <td><i>${message.tag}</i></td>
                        <td><a href="/">Edit</a> </td>
                    </tr>
                <#else>
                    <div>No messages</div>
                </#list>
            </tbody>
        </table>
    </div>
</@c.page>